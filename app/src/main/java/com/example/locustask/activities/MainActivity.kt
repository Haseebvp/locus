package com.example.locustask.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.example.locustask.BuildConfig
import com.example.locustask.R
import com.example.locustask.adapters.FormAdapter
import com.example.locustask.models.FormObject
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.lang.Exception
import com.example.locustask.models.FormData
import com.google.gson.Gson
import java.io.IOException


class MainActivity : AppCompatActivity() {
    companion object {
        const val PHOTO_CAPTURE = 100
    }

    lateinit var recyclerview: RecyclerView
    lateinit var layoutmanger: LinearLayoutManager
    lateinit var adapter: FormAdapter
    lateinit var imageURI: Uri
    var photoPosition: Int = 0
    lateinit var helper: FormHelper

    lateinit var datalist: ArrayList<FormObject>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

        tvSubmit.setOnClickListener(View.OnClickListener {
            //            datalist contains all the updated data to send to server
        })
    }

    private fun init() {
        val gson = Gson()
        helper = FormHelper()
        datalist = gson.fromJson(loadJSONFromAsset()!!, FormData::class.java).data
        recyclerview = rvForm
        adapter = FormAdapter(helper, datalist)
        layoutmanger = LinearLayoutManager(this@MainActivity, VERTICAL, false)
        recyclerview.layoutManager = layoutmanger
        recyclerview.adapter = adapter
        recyclerview.addItemDecoration(FormDecorator())
    }


    fun createImageFile(): File {
        val imagename = "Locus"
        val storageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera")
        val image = File.createTempFile(imagename, ".jpg", storageDir)
        return image

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            PHOTO_CAPTURE -> if (resultCode == Activity.RESULT_OK) {
                val seletedImage = imageURI
                helper.updatePhotoHolder(photoPosition, seletedImage)
            }
        }
    }

    inner class FormDecorator : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val spacing = resources.getDimensionPixelSize(R.dimen.dp_5)
            outRect.set(0, spacing, 0, spacing)
        }
    }

    private fun loadJSONFromAsset(): String? {
        var json: String? = null
        try {
            val ip = this.assets.open("data.json")
            val size = ip.available()
            val buffer = ByteArray(size)
            ip.read(buffer)
            ip.close()
            json = String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()

        }
        return json
    }

    inner class FormHelper {
        fun updatePhotoHolder(position: Int, seletedImage: Uri) {
            try {
                datalist[position].dataMap["selectedOption"] = arrayListOf(seletedImage.toString())
                adapter.notifyItemChanged(position)
            } catch (e: Exception) {

            }
        }

        fun updateChoiceHolder(position: Int, dataPosition: Int) {
            try {
                val optionlist = datalist[position].dataMap["options"]
                datalist[position].dataMap["selectedOption"] = arrayListOf(optionlist!![dataPosition].toString())
            } catch (e: Exception) {

            }
        }

        fun updateComments(position: Int, comment: String) {
            try {
                datalist[position].dataMap["selectedOption"] = arrayListOf(comment)
            } catch (e: Exception) {

            }
        }

        fun getContext() : Context{
            return this@MainActivity
        }

        fun openCamera(position: Int) {
            photoPosition = position
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val photo = createImageFile()
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo))
                imageURI = Uri.fromFile(photo)
            } else {
                val uri =
                    FileProvider.getUriForFile(applicationContext, BuildConfig.APPLICATION_ID + ".provider", photo)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
                imageURI = uri
            }
            startActivityForResult(intent, PHOTO_CAPTURE)
        }


    }

}
