package com.example.locustask.utils

import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import com.example.locustask.activities.MainActivity
import com.example.locustask.models.FormObject
import android.view.animation.Animation
import android.view.animation.Transformation


object AppUtils {
    fun getInitDataList(): ArrayList<FormObject>{
        val data:ArrayList<FormObject> = ArrayList<FormObject>()
        val data1 = FormObject("PHOTO","pic1","Photo 1", HashMap())
        val choice = HashMap<String, ArrayList<String>>()
        choice.put("options", arrayListOf("Good","Ok", "Bad"))
        val data2 = FormObject("SINGLE_CHOICE","choice1","Photo 1 choice", choice)
        val data3 = FormObject("COMMENT","comment1","Photo 1 comments", HashMap())
        data.add(data1)
        data.add(data2)
        data.add(data3)
        return data
    }

    fun expand(v : View){
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a = object : Animation() {
            protected override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    ViewGroup.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = ((targetHeight / v.context.resources.displayMetrics.density) as Int).toLong()
        v.startAnimation(a)

    }

    fun collapse(v:View){
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            protected override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = ((initialHeight / v.context.resources.displayMetrics.density) as Int).toLong()
        v.startAnimation(a)
    }
}