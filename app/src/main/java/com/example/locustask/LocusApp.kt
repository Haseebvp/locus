package com.example.locustask

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig

class LocusApp : Application() {
    override fun onCreate() {
        super.onCreate()
        val config: ImagePipelineConfig  = ImagePipelineConfig.newBuilder(this).setDownsampleEnabled(true).build()
        Fresco.initialize(this, config)
    }
}