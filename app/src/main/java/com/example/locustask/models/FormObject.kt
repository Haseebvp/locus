package com.example.locustask.models

data class FormObject(
    val type: String,
    val id: String,
    val title: String,
    val dataMap: HashMap<String, ArrayList<String>>
) {

    fun getCardType(): Int {
        return when {
            type.equals("PHOTO") -> 0
            type.equals("SINGLE_CHOICE") -> 1
            type.equals("COMMENT") -> 2
            else -> 0
        }
    }
}