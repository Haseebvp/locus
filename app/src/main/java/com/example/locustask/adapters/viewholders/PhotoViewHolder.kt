package com.example.locustask.adapters.viewholders

import android.content.Context
import android.net.Uri
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.locustask.activities.MainActivity
import com.example.locustask.models.FormObject
import kotlinx.android.synthetic.main.item_photo.view.*

class PhotoViewHolder(itemView: View) : BaseViewHolder(itemView) {
    override fun setUp(item: FormObject, helper: MainActivity.FormHelper, position: Int) {
        itemView.tvPhotoTitle.text = item.title
        itemView.icClose.visibility = View.GONE
        if (item.dataMap.containsKey("selectedOption")) {
            if (item.dataMap.get("selectedOption")!!.size > 0) {
                val uri = Uri.parse(item.dataMap.get("selectedOption")!!.get(0).toString())
                itemView.ivPhoto.setImageURI(uri)
                itemView.icClose.visibility = View.VISIBLE
            }
        }

        itemView.ivPhoto.setOnClickListener(View.OnClickListener {
            helper.openCamera(position)
        })
        itemView.icClose.setOnClickListener(View.OnClickListener {
            itemView.ivPhoto.setImageURI(Uri.EMPTY)
            helper.updatePhotoHolder(position, Uri.EMPTY)
        })
    }


}
