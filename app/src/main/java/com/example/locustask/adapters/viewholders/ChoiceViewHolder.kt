package com.example.locustask.adapters.viewholders

import android.content.Context
import android.net.Uri
import android.view.View
import android.widget.RadioGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.locustask.models.FormObject
import kotlinx.android.synthetic.main.item_choice.view.*
import kotlinx.android.synthetic.main.item_photo.view.*
import android.view.LayoutInflater
import com.example.locustask.R
import com.example.locustask.activities.MainActivity
import kotlinx.android.synthetic.main.item_radio_button.view.*
import kotlin.collections.ArrayList


class ChoiceViewHolder(inflate: View) : BaseViewHolder(inflate) {
    override fun setUp(item: FormObject, helper: MainActivity.FormHelper, position: Int) {
        itemView.tvChoiceTitle.text = item.title
        var optionslist = ArrayList<String>()
        if (item.dataMap.containsKey("options")) {
            if (item.dataMap.get("options")!!.size > 0) {
                optionslist.addAll(item.dataMap.get("options")!!)
                setupRadioGroup(itemView.rgChoice!!, item.dataMap, helper.getContext())
            }
        }

        if (item.dataMap.containsKey("selectedOption")) {
            if (!item.dataMap.get("selectedOption").isNullOrEmpty()) {
                val uri = Uri.parse(item.dataMap.get("selectedOption").toString())
                itemView.ivPhoto.setImageURI(uri)
                itemView.icClose.visibility = View.VISIBLE
            }
        }

        itemView.rgChoice.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener{group, checkedId ->
            helper.updateChoiceHolder(position, checkedId)
        })
    }


    private fun setupRadioGroup(
        rgChoice: RadioGroup?,
        map: HashMap<String, ArrayList<String>>,
        context: Context
    ) {
        rgChoice!!.removeAllViews()
        val list = map.get("options")
        for (i in 0 until list!!.size){
            val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val v = layoutInflater.inflate(R.layout.item_radio_button, null)
            v.radio.text = list.get(i)
            if (map.containsKey("selectedOption")){
                if (map.get("selectedOption")!!.size > 0){
                    if (map.get("selectedOption")!!.get(0).toString().equals(list.get(i))){
                        v.radio.isChecked = true
                    }
                }
            }
            v.radio.id = i
            rgChoice.addView(v)
        }
    }

}
