package com.example.locustask.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.locustask.R
import com.example.locustask.activities.MainActivity
import com.example.locustask.adapters.viewholders.ChoiceViewHolder
import com.example.locustask.adapters.viewholders.CommentViewHolder
import com.example.locustask.adapters.viewholders.PhotoViewHolder
import com.example.locustask.models.FormObject

class FormAdapter(private val helper: MainActivity.FormHelper, private val data: List<FormObject>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        var TYPE_PHOTO = 0
        var TYPE_CHOICE = 1
        var TYPE_COMMENT = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val holder: RecyclerView.ViewHolder
        when (viewType) {
            TYPE_PHOTO -> holder = PhotoViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_photo,
                    parent,
                    false
                )
            )
            TYPE_CHOICE -> holder = ChoiceViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_choice,
                    parent,
                    false
                )
            )
            TYPE_COMMENT -> holder = CommentViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_comment,
                    parent,
                    false
                )
            )
            else ->
                holder = PhotoViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_photo,
                        parent,
                        false
                    )
                )
        }
        return holder
    }

    override fun getItemCount(): Int {
        return data.size
    }



    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = data[position]
        when {
            holder.itemViewType == TYPE_PHOTO -> (holder as PhotoViewHolder).setUp(item, helper, position)
            holder.itemViewType == TYPE_CHOICE -> (holder as ChoiceViewHolder).setUp(item, helper, position)
            holder.itemViewType == TYPE_COMMENT -> (holder as CommentViewHolder).setUp(item, helper, position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return data[position].getCardType()
    }

}
