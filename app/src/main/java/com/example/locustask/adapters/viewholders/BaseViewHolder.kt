package com.example.locustask.adapters.viewholders

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.locustask.activities.MainActivity
import com.example.locustask.models.FormObject

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    abstract fun setUp(item: FormObject, helper: MainActivity.FormHelper, position: Int)

}
