package com.example.locustask.adapters.viewholders

import android.content.Context
import android.text.TextWatcher
import android.view.View
import android.widget.CompoundButton
import androidx.recyclerview.widget.RecyclerView
import com.example.locustask.models.FormObject
import com.example.locustask.utils.AppUtils
import kotlinx.android.synthetic.main.item_comment.view.*
import android.widget.Toast
import android.text.Editable
import com.example.locustask.activities.MainActivity


class CommentViewHolder(inflate: View) : BaseViewHolder(inflate) {
    override fun setUp(item: FormObject, helper: MainActivity.FormHelper, position: Int) {
        if (item.dataMap.containsKey("selectedOption")) {
            if (item.dataMap["selectedOption"]!!.size > 0) {
                itemView.etComment.setText(item.dataMap["selectedOption"]!![0])
            }
        }

        itemView.stComment.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                AppUtils.expand(itemView.etComment)
            } else {
                AppUtils.collapse(itemView.etComment)
            }
        }

        itemView.etComment.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(cs: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
            }

            override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
            }

            override fun afterTextChanged(arg0: Editable) {
                helper.updateComments(position, arg0.toString())
            }
        });
    }

}
